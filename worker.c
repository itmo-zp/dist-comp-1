#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include "worker.h"
#include "ipc.h"
#include "log.h"

static void waitWorkers(const Worker* worker, const MessageType type) {
    int received_workers_count = 0;
    int* received_workers = calloc((size_t) worker->worker_pipes_count, sizeof(int));
    Message* received_message = malloc(sizeof(Message));

    while (received_workers_count < worker->worker_pipes_count) {
        for (int i = 0; i < worker->worker_pipes_count; i++) {
            if (!received_workers[i]
                && receive((void *) worker, worker->worker_pipes[i].id, received_message) == 0
                && received_message->s_header.s_type == type) {
                received_workers[i] = 1;
                received_workers_count++;
            }
        }
    }

    free(received_workers);
    free(received_message);
}

static void prepare(const Worker* worker) {
    Message message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 48,
                    .s_type = STARTED,
                    .s_local_time = 0
            }
    };
    send_multicast((void *) worker, &message);

    waitWorkers(worker, STARTED);
}

static void doWork(const Worker* worker) {

}

static void finish(const Worker* worker) {
    Message message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 28,
                    .s_type = DONE,
                    .s_local_time = 0
            }
    };
    send_multicast((void *) worker, &message);

    waitWorkers(worker, DONE);
}

int start(const Worker* worker) {
    log_started(worker);
    prepare(worker);
    log_received_all_started(worker);
    doWork(worker);
    log_done(worker);
    finish(worker);
    log_received_all_done(worker);

    return 0;
}
