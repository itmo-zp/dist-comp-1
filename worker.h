#ifndef __IFMO_DISTRIBUTED_CLASS_WORKER__H
#define __IFMO_DISTRIBUTED_CLASS_WORKER__H

#include <sys/types.h>
#include "ipc.h"

typedef struct {
    local_id id;
    pid_t in;
    pid_t out;
} ProcessPipe;

typedef struct {
	local_id id;
    local_id worker_pipes_count;
    ProcessPipe* worker_pipes;
    ProcessPipe master_pipe;
} Worker;

int start(const Worker* worker);

#endif // __IFMO_DISTRIBUTED_CLASS_WORKER__H
