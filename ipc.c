#include <stdio.h>
#include "ipc.h"
#include "unistd.h"
#include "worker.h"

int send(void* self, local_id dst, const Message* msg) {
    Worker* worker = self;

    for (local_id i = 0; i < worker->worker_pipes_count; i++) {
        if (worker->worker_pipes[i].id == dst) {
            size_t nbyte = sizeof(MessageHeader) + msg->s_header.s_payload_len;
            return nbyte != write(
                    worker->worker_pipes[i].out,
                    msg,
                    nbyte
            );
        }
    }

    return -1;
}

int send_multicast(void* self, const Message* msg) {
    Worker* worker = self;
    size_t nbyte = sizeof(MessageHeader) + msg->s_header.s_payload_len;

    for (local_id i = 0; i < worker->worker_pipes_count; i++) {
        if (nbyte != write(
                worker->worker_pipes[i].out,
                msg,
                nbyte
        )) {
            return 1;
        }
    }

    return nbyte != write(
            worker->master_pipe.out,
            msg,
            nbyte
    );
}

int receive(void* self, local_id from, Message* msg) {
    Worker* worker = self;

    for (local_id i = 0; i < worker->worker_pipes_count; i++) {
        if (worker->worker_pipes[i].id == from) {
            size_t nbyte = sizeof(MessageHeader);
            return nbyte != read(
                    worker->worker_pipes[i].in,
                    &msg->s_header,
                    nbyte
            ) || msg->s_header.s_payload_len != read(
                    worker->worker_pipes[i].in,
                    msg->s_payload,
                    msg->s_header.s_payload_len
            );
        }
    }

    return -1;
}

int receive_any(void *self, Message *msg) {
    Worker *worker = self;
    size_t nbyte = sizeof(MessageHeader);

    for (local_id i = 0; i < worker->worker_pipes_count; i++) {
        if (nbyte == read(
                worker->worker_pipes[i].in,
                &msg->s_header,
                nbyte
        )) {
            return msg->s_header.s_payload_len != read(
                    worker->worker_pipes[i].in,
                    msg->s_payload,
                    msg->s_header.s_payload_len
            );
        }
    }

    return -1;
}
